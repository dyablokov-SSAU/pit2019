﻿namespace GenericComplex.Tests
{
    using System;
    using NUnit.Framework;
    using MathNet.Numerics;

    [TestFixture]
    public class MathNETTests
    {
        [Test]
        [Category("MathNET")]
        public void Complex32Test()
        {
            var z1 = new Complex32(14, 57);
            var z2 = new Complex32(36, 82);

            var qout = z1 / z2;
        }
    };
}
