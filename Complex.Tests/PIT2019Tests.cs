﻿namespace GenericComplex.Tests
{
    using System;
    using NUnit.Framework;
    using Deveel.Math;

    [TestFixture]
    public class PIT2019Tests
    {
        private const string CATEGORY = "PIT2019";

        [Test]
        [Category(CATEGORY)]
        public void ComplexInt32Test()
        {
            var z1 = Complex.Create(new Int32Number()).Set(14, 57);
            var z2 = Complex.Create(new Int32Number()).Set(36, 82);

            var qout = z1 / z2;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexSingleTest()
        {
            var z1 = Complex.Create(new SingleNumber()).Set(14, 57);
            var z2 = Complex.Create(new SingleNumber()).Set(36, 82);

            var qout = z1 / z2;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexDoubleTest()
        {
            var z1 = Complex.Create(new DoubleNumber()).Set(14, 57);
            var z2 = Complex.Create(new DoubleNumber()).Set(36, 82);

            var qout = z1 / z2;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexDecimalTest()
        {
            var z1 = Complex.Create(new DecimalNumber()).Set(14, 57);
            var z2 = Complex.Create(new DecimalNumber()).Set(36, 82);

            var qout = z1 / z2;
        }
        [Test]
        [Category(CATEGORY)]
        public void ComplexBigDecimalTest()
        {
            var z1 = Complex.Create(new BigDecimalNumber(128)).Set(14, 57);
            var z2 = Complex.Create(new BigDecimalNumber(128)).Set(36, 82);

            var qout = z1 / z2;
        }
    };
}
