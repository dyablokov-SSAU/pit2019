﻿namespace GenericComplex
{
    public static class Complex
    {
        public static Complex<T> Create<T>(INumber<T> cpt)
        {
            return new Complex<T>(cpt);
        }
        public static Complex<T> Create<T>(INumber<T> cpt, T re, T im)
        {
            return new Complex<T>(cpt) { Re = re, Im = im };
        }
        public static Complex<T> SetRe<T>(this Complex<T> complex, T value)
        {
            complex.Re = value;
            return complex;
        }
        public static Complex<T> SetIm<T>(this Complex<T> complex, T value)
        {
            complex.Im = value;
            return complex;
        }
        public static Complex<T> Set<T>(this Complex<T> complex, T re, T im)
        {
            complex.Re = re;
            complex.Im = im;
            return complex;
        }
    };

    public class Complex<T> : ComplexBase<Complex<T>, T>
    {
        protected override Complex<T> Create(T re, T im) => new Complex<T>(_cpt) { Re = re, Im = im };
        public Complex(INumber<T> cpt) => _cpt = cpt;

        public static Complex<T> operator + (Complex<T> lhs, Complex<T> rhs) => lhs.Add(rhs);
        public static Complex<T> operator - (Complex<T> lhs, Complex<T> rhs) => lhs.Sub(rhs);
        public static Complex<T> operator * (Complex<T> lhs, Complex<T> rhs) => lhs.Mul(rhs);
        public static Complex<T> operator / (Complex<T> lhs, Complex<T> rhs) => lhs.Div(rhs);
    };
}
