﻿namespace GenericComplex
{
    public interface IComplex<T>
    {
        T Re { get; set; }
        T Im { get; set; }
    };
}
