﻿namespace GenericComplex
{
    public abstract class ComplexBase<C, T> : IComplex<T>
        where C : IComplex<T>
    {
        protected INumber<T> _cpt;
        protected abstract C Create(T re, T im);

        protected virtual C Add(C other) => Create(_cpt.Add(Re, other.Re), _cpt.Add(Im, other.Im));
        protected virtual C Sub(C other) => Create(_cpt.Add(Re, _cpt.Neg(other.Re)), _cpt.Add(Im, _cpt.Neg(other.Im)));
        protected virtual C Mul(C other)
        {
            var re = _cpt.Add(_cpt.Mul(Re, other.Re), _cpt.Neg(_cpt.Mul(Im, other.Im)));
            var im = _cpt.Add(_cpt.Mul(Re, other.Im), _cpt.Mul(Im, other.Re));
            return Create(re, im);
        }
        protected virtual C Div(C other)
        {
            if (!_cpt.Lt(_cpt.Abs(other.Re), _cpt.Abs(other.Im)))
                return DivExt(Re, Im, other.Re, other.Im, false);
            return DivExt(Im, Re, other.Im, other.Re, true);
        }
        protected virtual C DivExt(T a, T b, T c, T d, bool neg)
        {
            var r = _cpt.Div(d, c);
            var t = _cpt.Div(_cpt.One, _cpt.Add(c, _cpt.Mul(d, r)));
            var re = DivExt(a, b, c, d, r, t);
            var im = DivExt(b, _cpt.Neg(a), c, d, r, t);
            return Create(re, neg ? _cpt.Neg(im) : im);
        }
        protected virtual T DivExt(T a, T b, T c, T d, T r, T t)
        {
            if (!_cpt.Et(r, _cpt.Zero))
            {
                var br = _cpt.Mul(b, r);
                if (!_cpt.Et(br, _cpt.Zero))
                    return _cpt.Mul(_cpt.Add(a, br), t);
                return _cpt.Add(_cpt.Mul(a, t), _cpt.Mul(_cpt.Mul(b, t), r));
            }
            return _cpt.Mul(_cpt.Add(a, _cpt.Mul(d, _cpt.Div(b, c))), t);
        }

        public T Re { get; set; }
        public T Im { get; set; }
    };
}
