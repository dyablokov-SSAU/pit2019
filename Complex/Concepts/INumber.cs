﻿namespace GenericComplex
{
    public interface INumber<T>
    {
        T Add(T arg1, T arg2);
        T Mul(T arg1, T arg2);
        T Div(T arg1, T arg2);
        bool Et(T arg1, T arg2);
        bool Lt(T arg1, T arg2);
        T Abs(T arg);
        T Neg(T arg);
        T Zero { get; }
        T One { get; }
    };
}
