﻿namespace GenericComplex
{
    public class DoubleNumber : INumber<double>
    {
        public virtual double Add(double arg1, double arg2) => arg1 + arg2;
        public virtual double Mul(double arg1, double arg2) => arg1 * arg2;
        public virtual double Div(double arg1, double arg2) => arg1 / arg2;
        public virtual bool Et(double arg1, double arg2) => arg1 == arg2;
        public virtual bool Lt(double arg1, double arg2) => arg1 < arg2;
        public virtual double Neg(double arg) => -arg;
        public virtual double Abs(double arg) => Lt(arg, Zero) ? Neg(arg) : arg;
        public virtual double Zero => 0;
        public virtual double One => 1;
    };
}
