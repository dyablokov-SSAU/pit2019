﻿namespace GenericComplex
{
    using Deveel.Math;

    public class BigDecimalNumber : INumber<BigDecimal>
    {
        private readonly MathContext _context;

        public BigDecimalNumber() => _context = MathContext.Unlimited;

        public BigDecimalNumber(int precision) => _context = new MathContext(precision);

        public virtual BigDecimal Add(BigDecimal arg1, BigDecimal arg2) => arg1.Add(arg2, _context);
        public virtual BigDecimal Mul(BigDecimal arg1, BigDecimal arg2) => arg1.Multiply(arg2, _context);
        public virtual BigDecimal Div(BigDecimal arg1, BigDecimal arg2) => arg1.Divide(arg2, _context);
        public virtual bool Et(BigDecimal arg1, BigDecimal arg2) => arg1.Equals(arg2);
        public virtual bool Lt(BigDecimal arg1, BigDecimal arg2) => arg1.CompareTo(arg2) < 1;
        public virtual BigDecimal Neg(BigDecimal arg) => arg.Negate(_context);
        public virtual BigDecimal Abs(BigDecimal arg) => Lt(arg, Zero) ? Neg(arg) : arg;
        public virtual BigDecimal Zero => BigDecimal.Zero;
        public virtual BigDecimal One => BigDecimal.One;
    };
    //public class BigDecimalNumber : INumber<BigDecimal>
    //{
    //    public virtual BigDecimal Add(BigDecimal arg1, BigDecimal arg2) => arg1 + arg2;
    //    public virtual BigDecimal Mul(BigDecimal arg1, BigDecimal arg2) => arg1 * arg2;
    //    public virtual BigDecimal Div(BigDecimal arg1, BigDecimal arg2) => arg1 / arg2;
    //    public virtual bool Et(BigDecimal arg1, BigDecimal arg2) => arg1 == arg2;
    //    public virtual bool Lt(BigDecimal arg1, BigDecimal arg2) => arg1 < arg2;
    //    public virtual BigDecimal Neg(BigDecimal arg) => -arg;
    //    public virtual BigDecimal Abs(BigDecimal arg) => Lt(arg, Zero) ? Neg(arg) : arg;
    //    public virtual BigDecimal Zero => BigDecimal.Zero;
    //    public virtual BigDecimal One => BigDecimal.One;
    //};
}
