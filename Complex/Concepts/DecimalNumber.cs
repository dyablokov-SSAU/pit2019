﻿namespace GenericComplex
{
    public class DecimalNumber : INumber<decimal>
    {
        public virtual decimal Add(decimal arg1, decimal arg2) => arg1 + arg2;
        public virtual decimal Mul(decimal arg1, decimal arg2) => arg1 * arg2;
        public virtual decimal Div(decimal arg1, decimal arg2) => arg1 / arg2;
        public virtual bool Et(decimal arg1, decimal arg2) => arg1 == arg2;
        public virtual bool Lt(decimal arg1, decimal arg2) => arg1 < arg2;
        public virtual decimal Neg(decimal arg) => -arg;
        public virtual decimal Abs(decimal arg) => Lt(arg, Zero) ? Neg(arg) : arg;
        public virtual decimal Zero => 0;
        public virtual decimal One => 1;
    };
}
