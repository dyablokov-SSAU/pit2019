﻿namespace GenericComplex
{
    public class Int32Number : INumber<int>
    {
        public virtual int Add(int arg1, int arg2) => arg1 + arg2;
        public virtual int Mul(int arg1, int arg2) => arg1 * arg2;
        public virtual int Div(int arg1, int arg2) => arg1 / arg2;
        public virtual bool Et(int arg1, int arg2) => arg1 == arg2;
        public virtual bool Lt(int arg1, int arg2) => arg1 < arg2;
        public virtual int Neg(int arg) => -arg;
        public virtual int Abs(int arg) => Mul(((arg >> 30) | One), arg);
        public virtual int Zero => 0;
        public virtual int One => 1;
    };
}
